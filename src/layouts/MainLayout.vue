<template>
  <q-layout class="has-background-image" view="lHh Lpr lFf">
    <q-header
      elevated
      class="text-contrast-primary"
    >
      <q-toolbar>
        <router-link to="/">
          <q-icon
            class="q-pr-xs"
            name="img:icons/WizardIcon-128.png"
            size="sm"
          />
        </router-link>
        <q-toolbar-title>
          <router-link class="plain-link" to="/">
            <div v-if="$q.screen.gt.xs">
              Wizard Point Counter
            </div>
            <div v-else>
              WPC
            </div>
          </router-link>
        </q-toolbar-title>
        <div>A digital tablet of truth</div>
      </q-toolbar>
    </q-header>

    <q-page-container>
      <router-view style="opacity: 0.92" />
    </q-page-container>

    <q-footer elevated>
      <q-toolbar class="text-contrast-primary">
        <router-link
          class="plain-link"
          to="/imprint"
        >
          Imprint
        </router-link>
      </q-toolbar>
    </q-footer>
  </q-layout>
</template>

<script lang="ts">
import { defineComponent } from 'vue'

/**
 * The main layout.
 */
export default defineComponent(
  {
    name: 'MainLayout'
  }
)
</script>

<style>
.has-background-image {
  background-image: url('src/assets/background.jpg');
  background-size: cover;
  background-position: bottom right;
}

.plain-link {
  color: inherit;
  text-decoration: none
}
</style>
