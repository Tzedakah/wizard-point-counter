#!/bin/sh
SCRIPT_DIR=$(dirname $(realpath "$0"))

# Stop and remove container wizard-point-counter if it exists
docker ps -a -q --filter "name=wizard-point-counter" | grep -q . \
  && docker stop wizard-point-counter \
  && docker rm wizard-point-counter

# Run wizard-point-counter
docker run \
  --name wizard-point-counter \
  -v $SCRIPT_DIR/..:/wizard-point-counter \
  -p 127.0.0.1:9000:9000 \
  -w /wizard-point-counter \
  wizard-point-counter /bin/bash -c 'npm install && quasar dev'
