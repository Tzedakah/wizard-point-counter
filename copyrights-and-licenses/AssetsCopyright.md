# Licenses of the assets
The following table provides an overview of the origin of different assets.
Also, the licenses under which they can be used is listed there.

* The first column contains the path under which the respective asset
  is stored in the project.
* The second column contains the licenses under which the respective asset 
  can be used.
* The third column contains a link to the respective asset.
* The fourth column contains the date at which the respective asset
  was downloaded
* The fifth column contains the artist
  to whom the respective asset belongs.

| path of asset             | license                   | link                                                                 | download date | artist                | 
|---------------------------|---------------------------|----------------------------------------------------------------------|---------------|-----------------------|
| src/assets/background.jpg | [Pixabay Content License] | https://pixabay.com/photos/forest-woods-trees-wooden-houses-7459553/ | 13.10.2024    | [Eynoxxart]           |
| src/assets/Sammy.jpg      | All Rights Reserved       | -                                                                    | 13.10.2024    | Samuel Graf & Friends |

## Acknowledgment
Thanks to everyone who shares his artwork freely with the community.
As someone who creates free content in his free time,
I know the time and energy this can consume.
Projects like this wouldn't be possible without you.

[Pixabay Content License]: PixabayContentLicense.md
[Eynoxart]: https://pixabay.com/users/eynoxart-17734450/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=7459553