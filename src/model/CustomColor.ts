/**
 * The custom colors defined for Wizard Point Counter.
 * These colors are directly available in the Wizard Point Counter
 * (can be used without custom css) in addition to the
 * <a href=https://quasar.dev/style/color-palette>
 *   quasar color palette
 * </a>.
 * The string value of every <code>CustomColor</code> specifies
 * how to reference to a <code>CustomColor</code>.
 *
 * <h4>Examples</h4>
 * The following example shows how to create an HTML element which displays
 * the text <em>Example</em> in the <code>CustomColor</code>
 * <code>CONTRAST_PRIMARY</code>.
 * ```html
 *   <div class="text-contrast-primary">
 *     Example
 *   </div>
 * ```
 *
 * The next example shows how to create a simple button which is colored
 * in the <code>CustomColor</code> <code>CONTRAST_PRIMARY</code>.
 * ```html
 *   <q-btn label="click me" color="contrast-primary" />
 * ```
 */
enum CustomColor {
  /**
   * A color which has a good contrast to the
   * <a href=https://quasar.dev/style/color-palette#brand-colors>
   *   primary color
   * </a>
   * of the Wizard Point Counter.
   */
  CONTRAST_PRIMARY = 'contrast-primary',

  /**
   * The pastel blue of the brand of the Wizard Point Counter.
   */
  PASTEL_BLUE = 'pastel-blue',

  /**
   * The pastel lavender of the brand of the Wizard Point Counter.
   */
  PASTEL_LAVENDER = 'pastel-lavender',

  /**
   * The pastel pink of the brand of the Wizard Point Counter.
   */
  PASTEL_PINK = 'pastel-pink',

  /**
   * The pastel orange of the brand of the Wizard Point Counter.
   */
  PASTEL_ORANGE = 'pastel-orange',

  /**
   * The pastel green of the brand of the Wizard Point Counter.
   */
  PASTEL_GREEN = 'pastel-green'
}