import { RouteRecordRaw } from 'vue-router'
import MainLayout from 'layouts/MainLayout.vue'
import HomePage from 'pages/HomePage.vue'
import ErrorNotFoundPage from 'src/pages/ErrorNotFound.vue'
import ImprintPage from 'pages/ImprintPage.vue'

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: MainLayout,
    children: [{ path: '', component: HomePage }]
  },
  {
    path: '/imprint',
    component: MainLayout,
    children: [{ path: '', component: ImprintPage }]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: ErrorNotFoundPage
  }
]

export default routes
