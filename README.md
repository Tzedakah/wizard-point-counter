# Wizard Point Counter

A digital tablet of truth

## Requirements
The easiest way of running Wizard Point Counter is by using Docker.
This guide assumes that Docker is installed and running.

## Run in development mode
Before running Wizard Point Counter for the first time in a new environment
execute the script `build.sh`.
```shell
sh docker/build.sh
```
To run Wizard Point Counter in development mode
execute the script `run.sh`.
```shell
sh docker/run.sh
```
Afterward, Wizard Point counter is available under `localhost:9000`.
The application runs with hot reloading enabled.
This means changes are immediately injected and in effect.
One does not need to restart the application for that.

## Deploy a preview
You can deploy every version of Wizard Point Counter to a separate website,
which is intended for previewing versions.
In the following, we explain how to build Wizard Point Counter
based on an arbitrarily chosen commit
and deploy the result to the preview website.
All you have to do is to run a pipeline from GitLab
for a branch that includes the chosen commit.
For that pipeline you have to define a custom variable
called `PREVIEW_COMMIT` which contains the hash of the chosen commit.
After the pipeline succeeded the version of Wizard Point Counter
defined by the chosen commit is available under:
https://wizardpointcounter-preview.web.app.

## Copyrights and acknowledgements
There is a [documentation of the asset licenses]
which also contains acknowledgements.

[documentation of the asset licenses]: copyrights-and-licenses/AssetsCopyright.md